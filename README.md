## Goal

### Automatically collect sentences, containing syntactic constructs with brackets from the web.

For example, from the following text


> Лазанья из тортильи? Бинго! Вместо листов пасты удобно использовать тонкие мексиканские лепешки: они более плотные, чем тонкий лаваш, и не дадут вытечь начинке, к тому же их не нужно отваривать, как пасту. **Идею быстрой лазаньи мы, признаемся честно, подсмотрели у Джейми Оливера: в любой мясной фарш добавляем обжаренный лук и морковь, отдельно обжариваем мелко нарезанные овощи (отлично подойдут кабачок или брокколи), если вы умеете готовить классический соус бешамель, это тоже будет кстати.** Собираем лазанью прямо в форме: мясной фарш, овощи, бешамель, можно сделать сколько угодно слоев. Не забудьте в завершение густо посыпать верхнюю лепешку тертым сыром — в духовке он расплавится, станет румяным и будет аппетитно тянуться за вилкой.

We want to acquire the sentence marked in bold:

> Идею быстрой лазаньи мы, признаемся честно, подсмотрели у Джейми Оливера: в любой мясной фарш добавляем обжаренный лук и морковь, отдельно обжариваем мелко нарезанные овощи (отлично подойдут кабачок или брокколи), если вы умеете готовить классический соус бешамель, это тоже будет кстати.

Because it contains a syntactic construct in brackets:

> ... (отлично подойдут кабачок или брокколи) ...


# Solution

Given a file of links separated by newline ('\n') ->

For each link:
* Push link to the set of visited links;
   * While set of visited links is not empty:
        * pop a link forom the set and retrieve the webpage content; 
        * extract cross-website links (links, which lead to the same domain or start with /), push to the set of visited links;
        * extract syntactic constructs which contain brackets `()` with some text in them;
        * push collected constructs to a set;
* Write collected constructs to a file;    
    
