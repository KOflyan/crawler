# coding=utf-8
import os

import requests as rq
import re


class RegexOperation:

    def __init__(self, content: str):
        self._content = content

    def sub(self, pattern: str, replace: str):
        self._content = re.sub(pattern, replace, self._content)
        return self

    def __str__(self):
        return self._content

    def __repr__(self):
        return self._content


class Crawler:

    def __init__(self, mode='online', dir=None):
        self.dir = dir
        self.mode = mode
        self._visited = set()
        self._to_visit = set()
        self._data = set()
        self._domain = self._proto =  self._site_name = ''
        self._match_regex = r"([а-яА-ЯёЁa-zA-ZäÄöÖüÜõÕžŽšŠ\s0-9,\-+&%:;'\"\!\?]+(?!\.)\s\n?\([а-яА-ЯёЁa-zA-ZäÄöÖüÜõÕžŽšŠ\s0-9,\.\-+&%:;'\"\!\?]{3,}\)\n?\t?[\S\s]+?\.)"

    def crawl(self) -> None:
        if self.mode == 'online':
            self._crawl()
        elif self.mode == 'local':
            self._crawl_local()

    def _crawl_local(self) -> None:
        for file_name in os.listdir(self.dir):
            with open(os.path.join(self.dir, file_name), 'r') as f:
                self._parse(f.read())

            with open(f"{file_name}_result.txt", 'w') as f:
                f.write(f"Constructs collected: {len(self._data)}\n\n\n")
                f.write(f'\n{"*" * 50}\n'.join(self._data))
            self._data = set()

    def _crawl(self) -> None:
        fr = open('links', 'r')
        for link in fr.readlines():
            link = link.strip()
            self._domain = '/'.join(link.split('/')[:3])
            self._site_name = self._get_website_name().split('.')[0]
            self._proto = re.findall(r'http.*://', link)[0]
            self._to_visit.add(link.strip())
            self._make_request_and_process_response()

            if len(self._data) == 0:
                continue
            with open(f'{self._site_name}.txt', 'w') as f:
                f.write(f"Links processed: {len(self._visited)}\n")
                f.write(f"Constructs collected: {len(self._data)}\n\n\n")
                f.write(f'\n{"*" * 50}\n'.join(self._data))

            self._to_visit = set()
            self._visited = set()
            self._data = set()
        print("Done")

    def _make_request_and_process_response(self) -> None:
        while len(self._to_visit) > 0 and len(self._visited) < 3000:
            link: str = self._to_visit.pop()
            if link in self._visited:
                continue
            self._visited.add(link)
            formatted_link = self._format_link(link)
            print(f'processing link {formatted_link}')
            try:
                self._get_content(rq.get(formatted_link))
            except Exception:
                pass
            print(len(self._data))
            print()

    def _get_content(self, r: rq.Response) -> None:
        try:
            self._parse(r.text)
        # Ignore media files (images etc)
        except UnicodeDecodeError:
            pass

    def _format_link(self, link) -> str:
        if link.startswith(self._domain):
            return link
        if link.startswith('//'):
            return f"{self._proto}{link[2:]}"
        if link.startswith('/'):
            return f"{self._domain}{link}"
        raise ValueError(f"Invalid link {link}")

    def _get_website_name(self) -> str:
        website = self._domain.split('://')[1]
        pieces = website.split('.')
        return '.'.join(pieces[1:] if website.startswith('www') else pieces)

    def _add_links_to_visit(self, content) -> None:
        self._to_visit.update(re.findall(f'({self._domain}|{self._proto}{self._site_name}.blogspot|/.*?)"', content))

    def _parse(self, content: str) -> None:
        content = RegexOperation(content.strip()) \
            .sub(r"<(script|noscript).*>(.|\n)*?</(script|noscript)>", '') \
            .sub(r"<(?!a)(.|\n)*?>", '') \
            .sub(r'(class=".*?"|a.*href=|target=.*|type=.*|title=.*)', '') \
            .sub(r"<!--.*-->", '') \
            .sub(r"\n\s*\n", "\n") \
            .sub(r"(  |\t)", '') \
            .__str__()

        if self.mode == 'online':
            self._add_links_to_visit(content)
        self._find_and_collect(str(RegexOperation(content).sub(r"<(.|\n)*?", '')))

    def _find_and_collect(self, content: str) -> None:
        if content is None:
            return
        self._data.update(set(re.findall(self._match_regex, content)))


if __name__ == '__main__':
    Crawler(
        # mode='local',
        # dir='files'
    ).crawl()


